const http = require('http');
const rp = require('request-promise');
const apiUrl = 'https://roulette-api.nowakowski-arkadiusz.com';

// Helper to make the code shorter
const post = (path, hashname = '', body = {}) => rp({
    method: 'POST',
    uri: apiUrl + path,
    body: body,
    json: true,
    headers: { 'Authorization': hashname }
});

// Helper to make the code shorter
const get = (path, hashname = '') => rp({
    method: 'GET',
    uri: apiUrl + path,
    json: true,
    headers: { 'Authorization': hashname }
});

//test('Type your tests here', () => {});
